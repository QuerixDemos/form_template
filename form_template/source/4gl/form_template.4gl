main

	DEFINE formName STRING
	
	CALL fgl_setenv("DBPATH","form")
	
	OPTIONS INPUT WRAP
	DEFER INTERRUPT
	#LET formName =  fgl_winprompt(0,0,"Enter Form File Name","",30,0)
	
	OPEN WINDOW wChooseForm WITH FORM "form/_all_templates_viewer"
	

	INPUT BY NAME formName WITHOUT DEFAULTS
		ON ACTION "ViewForm"
			OPEN WINDOW wForm WITH FORM formName
			MENU
				ON ACTION "Exit"
					EXIT MENU
			END MENU
			CLOSE WINDOW wForm	
	
	END INPUT

	
END MAIN
	